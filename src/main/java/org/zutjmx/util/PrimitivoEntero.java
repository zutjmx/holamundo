package org.zutjmx.util;

public class PrimitivoEntero {
    public PrimitivoEntero() {
    }

    public void generaPrimitivos() {
        System.out.println(":: Tipo de dato byte ::");
        byte numeroByte = 127;
        System.out.println("numeroByte = " + numeroByte);
        System.out.println("Tipo byte corresponde en en byte a: " + Byte.BYTES);
        System.out.println("Tipo byte corresponde en en bits a: " + Byte.SIZE);
        System.out.println("Valor máximo del tipo byte: " + Byte.MAX_VALUE);
        System.out.println("Valor mínimo del tipo byte: " + Byte.MIN_VALUE);

        System.out.println(":: Tipo de dato short ::");
        short numeroShort = 32767;
        System.out.println("numeroShort = " + numeroShort);
        System.out.println("Tipo short corresponde en en byte a: " + Short.BYTES);
        System.out.println("Tipo short corresponde en en bits a: " + Short.SIZE);
        System.out.println("Valor máximo del tipo short: " + Short.MAX_VALUE);
        System.out.println("Valor mínimo del tipo short: " + Short.MIN_VALUE);

        System.out.println(":: Tipo de dato int ::");
        int numeroInt = 2147483647;
        System.out.println("numeroInt = " + numeroInt);
        System.out.println("Tipo int corresponde en en byte a: " + Integer.BYTES);
        System.out.println("Tipo int corresponde en en bits a: " + Integer.SIZE);
        System.out.println("Valor máximo del tipo int: " + Integer.MAX_VALUE);
        System.out.println("Valor mínimo del tipo int: " + Integer.MIN_VALUE);

        System.out.println(":: Tipo de dato long ::");
        long numeroLong = 9223372036854775807L; //Se usa al final la L mayúscula para designar que la literal es de tipo long
        System.out.println("numeroLong = " + numeroLong);
        System.out.println("Tipo long corresponde en en byte a: " + Long.BYTES);
        System.out.println("Tipo long corresponde en en bits a: " + Long.SIZE);
        System.out.println("Valor máximo del tipo long: " + Long.MAX_VALUE);
        System.out.println("Valor mínimo del tipo long: " + Long.MIN_VALUE);

        System.out.println(":: Tipo de dato var (existe desde JDK >= 10) ::");
        var numeroVar = 86256108;
        System.out.println("numeroVar = " + numeroVar);
    }
}
