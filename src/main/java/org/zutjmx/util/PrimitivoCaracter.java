package org.zutjmx.util;

public class PrimitivoCaracter {
    public PrimitivoCaracter() {
    }

    public void generaPrimitivoCaracter() {
        System.out.println(":: Tipo de dato char ::");
        char caracter = '\u0040';
        char valorDecimal = 64;
        char simbolo = '@';
        char espacio = '\u0020';
        System.out.println("caracter = " + caracter);
        System.out.println("valorDecimal = " + valorDecimal);
        System.out.println("simbolo = " + simbolo);
        System.out.println("¿caracter es igual a valorDecimal? " + (caracter == valorDecimal? "verdadero":"falso"));
        System.out.println("¿simbolo es igual a valorDecimal? " + (simbolo == valorDecimal? "verdadero":"falso"));

        System.out.println("Tipo char corresponde en en byte a:" + espacio + Character.BYTES);
        System.out.println("Tipo char corresponde en en bits a:" + espacio  + Character.SIZE);
        System.out.println("Valor máximo del tipo char:" + espacio  + Character.MAX_VALUE);
        System.out.println("Valor mínimo del tipo char:" + espacio  + Character.MIN_VALUE);

        char retroceso = '\b';
        System.out.println("retroceso" + retroceso);

        char tabulador = '\t';
        System.out.println("Usando" + tabulador + "tabulador" );

        char nuevaLinea = '\n';
        System.out.println("Usando" + nuevaLinea + "nueva línea" );

        char retornoDeCarro = '\r';
        System.out.println("Usando" + retornoDeCarro + "sólo retorno de carro" );
        System.out.println("Usando" + nuevaLinea + retornoDeCarro + "nueva línea + retorno de carro" );

        String separadorDeLinea = System.getProperty("line.separator");
        System.out.println("Usando propiedad " + separadorDeLinea + "separadorDeLinea");

        String lineSeparator = System.lineSeparator();
        System.out.println("Usando método " + lineSeparator + "lineSeparator");
    }
}
