package org.zutjmx.util;

import com.github.javafaker.Faker;

public class PruebaRendimientoConcat {
    public PruebaRendimientoConcat() {
    }

    public void realizaPruebaRendimiento() {
        Faker faker = new Faker();
        String cadenaA = faker.lebowski().quote();
        String cadenaB = faker.lebowski().quote();
        String cadenaC = cadenaA;
        System.out.println("cadenaA = " + cadenaA);
        System.out.println("cadenaB = " + cadenaB);
        System.out.println("cadenaC = " + cadenaC);

        StringBuilder sb = new StringBuilder(cadenaA);
        long inicio = System.currentTimeMillis();
        System.out.println("inicio = " + inicio);
        for (int i = 0; i < 500; i++) {
            //cadenaC = cadenaC.concat(cadenaA).concat(cadenaB).concat("\n"); // 24 ms
            //cadenaC+= cadenaA + cadenaB + "\n"; // 24 ms
            sb.append(cadenaA).append(cadenaB).append("\n"); // 5 ms
        }
        long fin = System.currentTimeMillis();
        System.out.println("fin = " + fin);
        System.out.println("cadenaC = " + cadenaC);
        System.out.println("sb = " + sb);
        System.out.println("Tiempo total : " + (fin-inicio));
    }
}
