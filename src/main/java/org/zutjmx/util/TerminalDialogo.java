package org.zutjmx.util;

import java.util.InputMismatchException;
import java.util.Scanner;

public class TerminalDialogo {
    public TerminalDialogo() {
    }

    public void operacionesConTerminal() {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Escriba un número entero: ");
        //String capturaNumero = scanner.nextLine();
        try {

            //int numeroDecimal = Integer.parseInt(capturaNumero);
            int numeroDecimal = scanner.nextInt();

            String resultadoBinario = "Número binario de " + numeroDecimal + " es " + Integer.toBinaryString(numeroDecimal) + "\n";
            String resuluadoOctal ="Número octal de " + numeroDecimal + " es " + Integer.toOctalString(numeroDecimal) + "\n";
            String resultadoHexadecimal = "Número hexadecimal de " + numeroDecimal + " es " + Integer.toHexString(numeroDecimal);

            String respuesta = resultadoBinario;
            respuesta += resuluadoOctal;
            respuesta += resultadoHexadecimal;

            System.out.println("Resultado {" + respuesta + "}");

        } catch (NumberFormatException | InputMismatchException exception) {
            System.out.println("Se debe de escribir un número entero");
            operacionesConTerminal();
        }
    }
}
