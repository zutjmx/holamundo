package org.zutjmx.util;

import com.github.javafaker.Faker;
import com.github.javafaker.service.FakeValuesService;
import com.github.javafaker.service.RandomService;

import java.util.Locale;

public class MetodosDeCadena {

    private Faker faker = new Faker(new Locale("es-MX"));

    private FakeValuesService fakeValuesService = new FakeValuesService(new Locale("es-MX"), new RandomService());

    public MetodosDeCadena() {
    }

    public void algunosMetodosDeCadena() {
        String nombre = faker.name().fullName();
        String nombreEnMayusculas = nombre.toUpperCase();
        String copiaNombre = nombre;
        String lebowskiCharacter = faker.lebowski().character();
        System.out.println("nombre = " + nombre);
        System.out.println("nombre.length() = " + nombre.length());
        System.out.println("nombre.toUpperCase() = " + nombre.toUpperCase());
        System.out.println("nombre.toLowerCase() = " + nombre.toLowerCase());
        System.out.println("lebowskiCharacter = " + lebowskiCharacter);
        System.out.println("copiaNombre = " + copiaNombre);
        System.out.println("nombre.equals(lebowskiCharacter) = " + nombre.equals(lebowskiCharacter));
        System.out.println("nombre.equals(copiaNombre) = " + nombre.equals(copiaNombre));
        System.out.println("nombre.equalsIgnoreCase(nombreEnMayusculas) = " + nombre.equalsIgnoreCase(nombreEnMayusculas));
        System.out.println("nombre.compareTo(copiaNombre) = " + nombre.compareTo(copiaNombre));
        System.out.println("nombre.compareTo(nombreEnMayusculas) = " + nombre.compareTo(nombreEnMayusculas));
        System.out.println("nombre.compareToIgnoreCase(nombreEnMayusculas) = " + nombre.compareToIgnoreCase(nombreEnMayusculas));
        for (int i = 0; i < nombre.length(); i++) {
            System.out.println("nombre.charAt("+i+") = " + nombre.charAt(i));
        }
        System.out.println("nombre.substring(0,5) = " + nombre.substring(0, 5));
        System.out.println("nombre.substring(4) = " + nombre.substring(4));
    }

    public void masMetodosDeCadena() {
        String frase = faker.lorem().paragraph();
        System.out.println("frase = " + frase);
        System.out.println("frase.replace(\"a\",\"@\") = " + frase.replace("a", "@"));
        System.out.println("frase.indexOf('b') = " + frase.indexOf('b'));
        System.out.println("frase.lastIndexOf('c') = " + frase.lastIndexOf('c'));
        System.out.println("frase.contains(\"t\") = " + frase.contains("t"));
        String email = fakeValuesService.bothify("????##@gmail.com");
        System.out.println("email = " + email);
        String alphaNumericString = fakeValuesService.regexify("[a-z1-9]{10}");
        System.out.println("alphaNumericString = " + alphaNumericString);
        System.out.println("frase.startsWith(\"A\") = " + frase.startsWith("A"));
        System.out.println("frase.endsWith(\"r\") = " + frase.endsWith("."));
        alphaNumericString = "      ".concat(alphaNumericString).concat("      ");
        System.out.println("alphaNumericString = " + alphaNumericString);
        System.out.println("alphaNumericString sin espacios = " + alphaNumericString.trim());
    }

    public void deCadenaAArreglo() {
        String alphaNumericString = fakeValuesService.regexify("[a-zA-Z1-9]{30}");
        System.out.println("alphaNumericString.length() = " + alphaNumericString.length());
        System.out.println("alphaNumericString = " + alphaNumericString);
        System.out.println("alphaNumericString.toCharArray() = " + alphaNumericString.toCharArray());
        char[] arreglo = alphaNumericString.toCharArray();
        int longitudArreglo = arreglo.length;
        System.out.println("longitudArreglo = " + longitudArreglo);
        for (int i = 0; i < longitudArreglo; i++) {
            System.out.println("arreglo["+i+"] = " + arreglo[i]);
        }
        String frase = faker.lorem().paragraph();
        System.out.println("frase = " + frase);
        String[] arregloDeCadenas = frase.split(" ");
        int longitudArregloDeCadenas = arregloDeCadenas.length;
        System.out.println("longitudArregloDeCadenas = " + longitudArregloDeCadenas);
        for (int i = 0; i < longitudArregloDeCadenas; i++) {
            System.out.println("arregloDeCadenas["+i+"] = " + arregloDeCadenas[i]);
        }
        String nombreArchivo = faker.file().fileName();
        System.out.println("nombreArchivo = " + nombreArchivo);
        String[] arregloArchivo = nombreArchivo.split("\\.");
        for (int i = 0; i < arregloArchivo.length; i++) {
            System.out.println("arregloArchivo["+i+"] = " + arregloArchivo[i]);
        }
    }
}
