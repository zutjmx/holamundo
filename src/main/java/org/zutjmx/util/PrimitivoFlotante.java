package org.zutjmx.util;

public class PrimitivoFlotante {
    static float campoFloat;
    public PrimitivoFlotante() {
    }

    public void generaFlotantes() {
        System.out.println(":: Tipo de dato float ::");
        //float numeroRealFloat = 3.1416f; //Con expansión decimal se debe colocar una F o f al final para indicar que es un flotante
        float numeroRealFloat = 2.12e3f; //Notación científica.
        float numeroRealFloatAux = 1.2e-3f; //Desplazamiento del cero a la izquierda.
        System.out.println("numeroRealFloat = " + numeroRealFloat);
        System.out.println("numeroRealFloatAux = " + numeroRealFloatAux);
        System.out.println("Tipo float corresponde en en byte a: " + Float.BYTES);
        System.out.println("Tipo float corresponde en en bits a: " + Float.SIZE);
        System.out.println("Valor máximo del tipo float: " + Float.MAX_VALUE);
        System.out.println("Valor mínimo del tipo float: " + Float.MIN_VALUE);

        System.out.println(":: Tipo de dato double ::");
        double numeroRealDouble = 1.7976931348623157E308;
        System.out.println("numeroRealDouble = " + numeroRealDouble);
        System.out.println("Tipo double corresponde en en byte a: " + Double.BYTES);
        System.out.println("Tipo double corresponde en en bits a: " + Double.SIZE);
        System.out.println("Valor máximo del tipo double: " + Double.MAX_VALUE);
        System.out.println("Valor mínimo del tipo double: " + Double.MIN_VALUE);

        var numeroVarFlotante = 3.1416f;
        System.out.println("numeroVarFlotante = " + numeroVarFlotante);
        System.out.println("campoFloat = " + campoFloat); //Los atributos de clase no se necesitan inicializar, sólo los de método.
    }
}
