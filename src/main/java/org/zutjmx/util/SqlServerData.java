package org.zutjmx.util;

import com.microsoft.sqlserver.jdbc.SQLServerDataSource;

import java.io.FileReader;
import java.io.IOException;
import java.sql.*;
import java.util.Properties;

public class SqlServerData {

    private static String connectionUrl = "jdbc:sqlserver://LAPTOP-T60KSR7R:1433;encrypt=true;trustServerCertificate=true;databaseName=AdventureWorks2019;user=sa;password=sistemas";

    public SqlServerData() {
    }

    public void getDatosPersona() {
        try (Connection con = DriverManager.getConnection(connectionUrl); Statement stmt = con.createStatement();) {
            String SQL = "SELECT TOP 10 * FROM Person.Person";
            ResultSet rs = stmt.executeQuery(SQL);

            // Iterate through the data in the result set and display it.
            while (rs.next()) {
                System.out.println(rs.getString("FirstName") + " " + rs.getString("LastName"));
            }
        }
        // Handle any errors that may have occurred.
        catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public void getEmployeeManagers() {
        /*Properties properties = new Properties();
        try {
            properties.load(new FileReader("config.properties"));
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
        System.out.println("properties.getProperty(\"usuario\") = " + properties.getProperty("usuario"));*/
        // Create datasource.
        SQLServerDataSource ds = new SQLServerDataSource();
        ds.setUser("sa");
        ds.setPassword("sistemas");
        ds.setServerName("LAPTOP-T60KSR7R");
        ds.setPortNumber(1433);
        ds.setDatabaseName("AdventureWorks2019");
        ds.setTrustServerCertificate(true);

        try (Connection con = ds.getConnection();
             CallableStatement cstmt = con.prepareCall("{call dbo.uspGetEmployeeManagers(?)}");) {
            // Execute a stored procedure that returns some data.
            cstmt.setInt(1, 50);
            ResultSet rs = cstmt.executeQuery();

            // Iterate through the data in the result set and display it.
            while (rs.next()) {
                System.out.println("EMPLOYEE: " + rs.getString("LastName") + ", " + rs.getString("FirstName"));
                System.out.println("MANAGER: " + rs.getString("ManagerLastName") + ", " + rs.getString("ManagerFirstName"));
                System.out.println();
            }
        }
        // Handle any errors that may have occurred.
        catch (SQLException e) {
            e.printStackTrace();
        }
    }
}
