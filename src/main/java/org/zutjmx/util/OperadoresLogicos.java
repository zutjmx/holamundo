package org.zutjmx.util;

import com.github.javafaker.Faker;

public class OperadoresLogicos {

    private Faker faker = new Faker();

    public OperadoresLogicos() {
    }

    public void manejoOperadoresLogicos() {
        int i = faker.number().numberBetween(1,10);
        System.out.println("i = " + i);
        int j = faker.number().numberBetween(11,20);
        System.out.println("j = " + j);
        float k = faker.number().numberBetween(1000,2000)*0.000025f;
        System.out.println("k = " + k);
        double l = faker.number().randomDouble(3,100,1000);
        System.out.println("l = " + l);
        boolean m = false;
        System.out.println("m = " + m);
        boolean b1 = i == j && k > l;
        System.out.println("b1 = i == j && k > l = " + b1);
        boolean b2 = i == j && k < l && m == false;
        System.out.println("b2 = i == j && k < l && m == false = " + b2);
        boolean b3 = i == j || k < l || m == false;
        System.out.println("b3 = i == j || k < l || m == false = " + b3);
        boolean b4 = i == j && k > l || m == false;
        System.out.println("b4 = i == j && k > l || m == false = " + b4);
        boolean b5 = i == j && (k > l || m == false);
        System.out.println("b5 = i == j && (k > l || m == false) = " + b5);
    }
}
