package org.zutjmx.util;

import java.util.Scanner;

public class Fibonacci {
    public Fibonacci() {
    }

    public void ejecutaSerie() {
        // Crear un objeto Scanner para leer la entrada del usuario
        Scanner sc = new Scanner(System.in);

        // Pedir al usuario que introduzca el número de términos que desea generar
        System.out.println("¿Cuántos términos de la serie de Fibonacci quieres generar?");

        // Leer el número introducido por el usuario y almacenarlo en una variable entera
        int n = sc.nextInt();

        // Cerrar el objeto Scanner
        sc.close();

        // Llamar al método que genera la serie de Fibonacci y pasarle el número de términos como argumento
        generaSerie(n);
    }

    public int getFib(int n) {
        if(n == 0 || n == 1) {
            return n;
        }
        return getFib(n - 1) + getFib(n - 2);
    }

    private void generaSerie(int n) {
        // Inicializar las variables que almacenan los dos primeros términos de la serie
        int a = 0; // Primer término
        int b = 1; // Segundo término

        // Imprimir el primer término
        System.out.print(a + " ");

        // Iterar desde el tercer término hasta el n-ésimo término
        for (int i = 3; i <= n; i++) {

            // Calcular el siguiente término como la suma de los dos anteriores
            int c = a + b;

            // Imprimir el siguiente término
            System.out.print(c + " ");

            // Actualizar las variables que almacenan los dos últimos términos
            a = b; // El segundo término se convierte en el primero
            b = c; // El siguiente término se convierte en el segundo
        }

        // Imprimir un salto de línea al final de la serie
        System.out.println();
    }
}
