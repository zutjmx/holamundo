package org.zutjmx.util;

import com.github.javafaker.Faker;

public class ValidaCadena {
    public ValidaCadena() {
    }

    public void validarCadenas() {
        Faker faker = new Faker();
        String curso = null;
        boolean cursoEsNulo = curso == null;
        System.out.println("cursoEsNulo = " + cursoEsNulo);

        if (cursoEsNulo) {
            curso = faker.commerce().productName();
        }
        System.out.println("curso.toUpperCase() = " + curso.toUpperCase());
        System.out.println("Concatenando usando '+' : " + curso); // Así no da error de NullPointerException si curso es null
        System.out.println("Concatenando usando 'concat' : ".concat(curso)); // Así da error de NullPointerException si curso es null

        System.out.println("Cadena vacía.");
        String cadenaVacia = "";
        //boolean esCadenaVacia = cadenaVacia.length() == 0;
        boolean esCadenaVacia = cadenaVacia.isEmpty();
        System.out.println("esCadenaVacia = " + esCadenaVacia);
        String cadenaConEspaciosEnBlanco = "    ";
        boolean esEspacioEnBlanco = cadenaConEspaciosEnBlanco.isBlank();
        System.out.println("esEspacioEnBlanco = " + esEspacioEnBlanco);
    }
}
