package org.zutjmx.util;

import com.github.javafaker.Faker;

public class OperadoresIncrementales {

    private Faker faker = new Faker();

    public OperadoresIncrementales() {
    }

    public void manejoDeOperadorIncremento() {
        System.out.println("Pre-incremento");
        int i = faker.number().numberBetween(1,10);
        System.out.println("i = " + i);
        int j = ++i;
        System.out.println("++i = " + i);
        System.out.println("j = ++i = " + j);
        System.out.println("Post-incremento");
        i = faker.number().numberBetween(11,20);
        System.out.println("i = " + i);
        j = i++;
        System.out.println("i++ = " + i);
        System.out.println("j = i++ = " + j);

        System.out.println("Jugando con j");
        System.out.println("++j = " + (++j));
        System.out.println("j++ = " + (j++));
        System.out.println("j = " + j);
    }

    public void manejoDeOperadorDecremento() {
        System.out.println("Pre-decremento");
        int i = faker.number().numberBetween(1,10);
        System.out.println("i = " + i);
        int j = --i;
        System.out.println("--i = " + i);
        System.out.println("j = --i = " + j);
        System.out.println("Post-decremento");
        i = faker.number().numberBetween(11,20);
        System.out.println("i = " + i);
        j = i--;
        System.out.println("i-- = " + i);
        System.out.println("j = i-- = " + j);
    }
}
