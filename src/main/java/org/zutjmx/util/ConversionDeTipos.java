package org.zutjmx.util;

public class ConversionDeTipos {
    public ConversionDeTipos() {
    }

    public void convierteTipoDeDato() {
        String enteroCadena = "550";
        System.out.println("enteroCadena = " + enteroCadena);
        int numeroEntero = Integer.parseInt(enteroCadena);
        System.out.println("numeroEntero = " + numeroEntero);

        String realCadena = "78909.56e-3";
        System.out.println("realCadena = " + realCadena);
        double numeroReal = Double.parseDouble(realCadena);
        System.out.println("numeroReal = " + numeroReal);

        String cadenaFalse = "false";
        String cadenaTrue = "true";
        boolean logicoFalse = Boolean.parseBoolean(cadenaFalse);
        System.out.println("logicoFalse = " + logicoFalse);
        boolean logicoTrue = Boolean.parseBoolean(cadenaTrue);
        System.out.println("logicoTrue = " + logicoTrue);

        int enteroNumero = 200;
        System.out.println("enteroNumero = " + enteroNumero);
        String cadenaEntero = Integer.toString(enteroNumero);
        System.out.println("cadenaEntero = " + cadenaEntero);
        cadenaEntero = String.valueOf(enteroNumero+90);
        System.out.println("cadenaEntero con String.valueOf(enteroNumero+90) = " + cadenaEntero);

        String cadenaReal = Double.toString(24569.78e-5);
        System.out.println("cadenaReal usando Double.toString(24569.78e-5) = " + cadenaReal);
        cadenaReal = String.valueOf(456789.67f);
        System.out.println("cadenaReal usando String.valueOf(456789.67f) = " + cadenaReal);

        int i = Short.MAX_VALUE;
        System.out.println("i = " + i);
        short s = (short) i;
        System.out.println("s = " + s);
        long l = i;
        System.out.println("l = " + l);
        char c = (char) i;
        System.out.println("c = " + c);
    }
}
