package org.zutjmx.util;

import com.github.javafaker.Faker;

import java.util.Locale;

public class ManejoDeCadena {

    private static final String ES_MX = "es-MX";
    public ManejoDeCadena() {
    }

    public void generaCadenas() {
        System.out.println(":Compara dos cadenas con == y equals:");
        Faker faker = new Faker(new Locale(ES_MX));
        String nombreUno = faker.name().fullName();
        System.out.println("nombreUno = " + nombreUno);
        String nombreDos = new String(nombreUno);
        System.out.println("nombreDos = " + nombreDos);

        if (nombreUno == nombreDos) {
            System.out.println("Son iguales nombreUno y nombreDos");
        } else {
            System.out.println("No son iguales nombreUno y nombreDos");
        }

        if(nombreUno.equals(nombreDos)) {
            System.out.println("nombreUno y nombreDos tienen el mismo valor");
        }

        String cursoUno = faker.book().title();
        System.out.println("cursoUno = " + cursoUno);
        String cursoDos = new String(cursoUno.toUpperCase());
        System.out.println("cursoDos = " + cursoDos);

        boolean esIgual = cursoUno == cursoDos;
        System.out.println("cursoUno == cursoDos : " + esIgual);
        esIgual = cursoUno.equals(cursoDos);
        System.out.println("cursoUno.equals(cursoDos) : " + esIgual);
        esIgual = cursoUno.equalsIgnoreCase(cursoDos);
        System.out.println("cursoUno.equalsIgnoreCase(cursoDos) : " + esIgual);

        String cursoTres = cursoUno;
        esIgual = cursoUno == cursoTres;
        System.out.println("cursoUno == cursoTres => " + esIgual);
    }

    public void concatenaCadena() {
        System.out.println(": Usando método concat() :");
        Faker faker = new Faker(new Locale(ES_MX));
        String nombreDePila = faker.name().firstName();
        System.out.println("nombreDePila = " + nombreDePila);
        String apellidoPaterno = faker.name().lastName();
        System.out.println("apellidoPaterno = " + apellidoPaterno);
        String apellidoMaterno = faker.name().lastName();
        System.out.println("apellidoMaterno = " + apellidoMaterno);
        String nombreCompleto = nombreDePila
                .concat(" ")
                .concat(apellidoPaterno)
                .concat(" ")
                .concat(apellidoMaterno);

        System.out.println("nombre completo = ".concat(nombreCompleto));

        System.out.println(": Usando operador + :");
        String titulo = faker.book().title();
        System.out.println("titulo = " + titulo);
        String autor = faker.book().author();
        System.out.println("autor = " + autor);
        String editorial = faker.book().publisher();
        System.out.println("editorial = " + editorial);
        String genero = faker.book().genre();
        System.out.println("genero = " + genero);
        String publicacion = "Publicación: " + titulo + ", " + autor + ", " + editorial + ", " + genero + ", ";
        System.out.println(publicacion);

        int numeroA = faker.number().randomDigit();
        System.out.println("numeroA = " + numeroA);
        int numeroB = faker.number().randomDigit();
        System.out.println("numeroB = " + numeroB);
        System.out.println(publicacion + numeroA + numeroB);
        System.out.println(publicacion + (numeroA + numeroB));
        System.out.println(numeroA + numeroB + ", " + publicacion);
    }
}
