package org.zutjmx.util;

import com.github.javafaker.Faker;

public class OperadoresAritmeticos {

    private Faker faker = new Faker();

    public OperadoresAritmeticos() {
    }

    public void manejoDeOperadores () {
        int numeroUno = faker.number().numberBetween(20,30), numeroDos = faker.number().numberBetween(11,19), suma = numeroUno + numeroDos, resta = numeroUno - numeroDos;
        System.out.println("numeroUno = " + numeroUno);
        System.out.println("numeroDos = " + numeroDos);
        System.out.println("suma = " + suma);
        System.out.println("numeroUno + numeroDos = " + numeroUno + numeroDos); // Concatenación de cadenas.
        System.out.println(numeroUno + numeroDos + " = numeroUno + numeroDos"); // Si los enteros van al principio, primero se hace la operación aritmética
        System.out.println("numeroUno + numeroDos = " + (numeroUno + numeroDos)); // La suma se pone entre () para la prioridad
        System.out.println("resta = " + resta);
        System.out.println("numeroUno - numeroDos = " + (numeroUno - numeroDos)); // Concatenación de cadenas.
        int multiplicacion = numeroUno * numeroDos;
        System.out.println("multiplicacion = " + multiplicacion);
        System.out.println("numeroUno * numeroDos = " + numeroUno * numeroDos);
        float div = (float) numeroUno / numeroDos; // Se hace un cast para no perder los decimales
        System.out.println("div = " + div);
        int resto = numeroUno % numeroDos;
        System.out.println("resto = " + resto);
        int restoOchoYCinco = 8 % 5;
        System.out.println("restoOchoYCinco (8 % 5) = " + restoOchoYCinco);
        int numero = faker.number().numberBetween(1,1000);
        if (numero % 2 == 0) {
            System.out.println("numero par = " + numero);
        } else {
            System.out.println("numero impar = " + numero);
        }
    }
}
