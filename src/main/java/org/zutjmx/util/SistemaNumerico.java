package org.zutjmx.util;

import com.github.javafaker.Faker;

public class SistemaNumerico {
    public SistemaNumerico() {
    }

    public void generaNumerico() {
        Faker faker = new Faker();
        int numeroDecimal = faker.number().numberBetween(500,599);
        System.out.println("numeroDecimal = " + numeroDecimal);

        System.out.println("Número binario de " + numeroDecimal + " es " + Integer.toBinaryString(numeroDecimal));
        System.out.println("Número octal de " + numeroDecimal + " es " + Integer.toOctalString(numeroDecimal));
        System.out.println("Número hexadecimal de " + numeroDecimal + " es " + Integer.toHexString(numeroDecimal));

        int numeroBinario = 0b1000010111; //Se antepone 0b para su representación binaria
        System.out.println("numeroBinario = " + numeroBinario);

        int numeroOctal = 01054; //Se antepone 0 para su representación octal
        System.out.println("numeroOctal = " + numeroOctal);

        int numeroHexadecimal = 0x21a; //Se antepone 0x para su representación hexadecimal
        System.out.println("numeroHexadecimal = " + numeroHexadecimal);
    }
}
