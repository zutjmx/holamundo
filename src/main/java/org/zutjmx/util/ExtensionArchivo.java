package org.zutjmx.util;

import com.github.javafaker.Faker;

import java.util.Locale;

public class ExtensionArchivo {

    private Faker faker = new Faker(new Locale("es-MX"));

    public ExtensionArchivo() {
    }

    public void generaExtensionArchivo() {
        String nombreArchivo = faker.file().fileName();
        int indiceDelPunto = nombreArchivo.lastIndexOf('.');
        System.out.println("nombreArchivo = " + nombreArchivo);
        System.out.println("nombreArchivo.length() = " + nombreArchivo.length());
        System.out.println("nombreArchivo.substring(indiceDelPunto) = " + nombreArchivo.substring(indiceDelPunto));
    }
}
