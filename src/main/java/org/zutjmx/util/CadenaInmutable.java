package org.zutjmx.util;

import com.github.javafaker.Faker;

public class CadenaInmutable {
    public CadenaInmutable() {
    }

    public void generaCadenaInmutable() {
        Faker faker = new Faker();
        String curso = faker.book().title();
        String profesor = faker.name().fullName();
        String resultado = curso.concat(". " + profesor);
        System.out.println("resultado = " + resultado);
        System.out.println("curso = " + curso);
        System.out.println("curso es igual a resultado: " + curso == resultado);

        System.out.println("Usando transform");
        String cursoTransformado = curso.transform(c -> {
            return c.concat(", " + profesor);
        });
        System.out.println("cursoTransformado = " + cursoTransformado);
        System.out.println("curso = " + curso);

        System.out.println("Usando replace");
        String cadenaReplace = resultado.replace("a", "A");
        System.out.println("cadenaReplace = " + cadenaReplace);
    }
}
