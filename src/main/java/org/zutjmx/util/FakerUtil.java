package org.zutjmx.util;

import com.github.javafaker.Faker;

public class FakerUtil {
    private final Faker faker;

    public FakerUtil(Faker faker) {
        this.faker = faker;
    }

    public String getNombreArtista() {
        return this.faker.artist().name();
    }

    public String getNombreCompleto() {
        return this.faker.name().fullName();
    }
}
