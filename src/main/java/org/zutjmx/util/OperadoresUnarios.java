package org.zutjmx.util;

import com.github.javafaker.Faker;

public class OperadoresUnarios {

    private Faker faker = new Faker();
    public OperadoresUnarios() {
    }

    public void manejoDeOperadoresUnarios() {
        int i = faker.number().numberBetween(-10,-1);
        System.out.println("i = " + i);
        int j = +i;
        System.out.println("j = " + j);
        int k = -i;
        System.out.println("k = " + k);
        i = faker.number().numberBetween(1,6);
        System.out.println("i = " + i);
        j = +i;
        System.out.println("j = " + j);
        k = -i;
        System.out.println("k = " + k);
    }
}
