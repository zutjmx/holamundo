package org.zutjmx.util;

import com.github.javafaker.Faker;

public class OperadoresRelacionales {

    private Faker faker = new Faker();

    public OperadoresRelacionales() {
    }

    public void manejoDeOperadoresRelacionales() {
        int i = faker.number().numberBetween(1,10);
        System.out.println("i = " + i);
        int j = faker.number().numberBetween(11,20);
        System.out.println("j = " + j);
        float k = faker.number().numberBetween(1000,2000)*0.000025f;
        System.out.println("k = " + k);
        double l = faker.number().randomDouble(3,100,1000);
        System.out.println("l = " + l);
        boolean m = false;
        System.out.println("m = " + m);
        boolean b1 = i == j;
        System.out.println("b1 = " + b1);
        boolean b2 = !b1; //negación de b
        System.out.println("b2 = !b1 = " + b2);
        boolean b3 = i != j;
        System.out.println("b3 = i != j = " + b3);
        boolean b4 = m == true;
        System.out.println("b4 = m == true = " + b4);
        boolean b5 = m != true;
        System.out.println("b5 = m != true = " + b5);
        boolean b6 = i > j;
        System.out.println("b6 = i > j = " + b6);
        boolean b7 = i < j;
        System.out.println("b7 = i < j = " + b7);
        boolean b8 = l >= k;
        System.out.println("b8 = l >= k = " + b8);
        boolean b9 = l <= k;
        System.out.println("b9 = l <= k = " + b9);
    }
}
