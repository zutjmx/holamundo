package org.zutjmx.util;

import javax.swing.*;

public class VentanaDialogo {
    public VentanaDialogo() {
    }

    public void operacionesConVentana() {
        String capturaNumero = JOptionPane.showInputDialog(null,"Capture un número entero");
        try {
            if ( capturaNumero == null
                    || capturaNumero.isBlank()
                    || capturaNumero.isEmpty()) {
                JOptionPane.showMessageDialog(null, "Se debe capturar un número");
                operacionesConVentana();
                return;
            }

            int numeroDecimal = Integer.parseInt(capturaNumero);

            String resultadoBinario = "Número binario de " + numeroDecimal + " es " + Integer.toBinaryString(numeroDecimal) + "\n";
            String resuluadoOctal ="Número octal de " + numeroDecimal + " es " + Integer.toOctalString(numeroDecimal) + "\n";
            String resultadoHexadecimal = "Número hexadecimal de " + numeroDecimal + " es " + Integer.toHexString(numeroDecimal);

            String respuesta = resultadoBinario;
            respuesta += resuluadoOctal;
            respuesta += resultadoHexadecimal;

            JOptionPane.showMessageDialog(null, respuesta);

        } catch (NumberFormatException e) {
            JOptionPane.showMessageDialog(null,"No se capturó un número: ".concat(e.toString()));
            operacionesConVentana();
        }
    }
}
