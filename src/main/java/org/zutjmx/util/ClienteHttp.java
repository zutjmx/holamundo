package org.zutjmx.util;

import kong.unirest.HttpResponse;
import kong.unirest.Unirest;
import okhttp3.*;

import java.io.IOException;

public class ClienteHttp {
    private final String URL_JSERVICE = "http://jservice.io/api";

    private final String URL_BUSQUEDA_DOMINIOS ="https://api.domainsdb.info/v1/domains/search";

    private final String URL_NBA_PLAYERS = "https://free-nba.p.rapidapi.com/players";

    public ClienteHttp() {
    }

    public String recuperaPistas() throws IOException {
        OkHttpClient client = new OkHttpClient();

        Request request = new Request.Builder()
                .url(URL_JSERVICE.concat("/clues"))
                .build();

        try (Response response = client.newCall(request).execute()) {
            return response.body().string();
        }
    }

    public String recuperaNbaPlayers() {
        OkHttpClient client = new OkHttpClient();

        Request request = new Request.Builder()
                .url(URL_NBA_PLAYERS)
                .addHeader("X-RapidAPI-Host", "free-nba.p.rapidapi.com")
                .addHeader("X-RapidAPI-Key", "LYMgkDcR89mshKoWQmQUSgMVGWVGp10jeedjsnL265vupRfG36")
                .build();

        try (Response response = client.newCall(request).execute()) {
            return response.body().string();
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    public HttpResponse<String> busquedaDominios() {

        HttpResponse<String> response = Unirest.get(URL_BUSQUEDA_DOMINIOS.concat("?domain=linux&zone=com"))
                .asString();

        return response;
    }
}
