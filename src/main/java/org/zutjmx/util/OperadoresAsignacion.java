package org.zutjmx.util;

import com.github.javafaker.Faker;
import org.zutjmx.models.Persona;

import java.util.Locale;

public class OperadoresAsignacion {

    private Faker faker = new Faker(new Locale("es-MX"));

    public OperadoresAsignacion() {
    }

    public void manejoDeOperadoresDeAsignacion() {
        Persona persona = new Persona();
        persona.setNombrePila(faker.name().firstName());
        persona.setApellidoPaterno(faker.name().lastName());
        persona.setApellidoMaterno(faker.name().lastName());
        persona.setFechaNacimiento(faker.date().birthday(20,90));
        System.out.println("persona.toString() = " + persona.toString());
        int i = faker.number().numberBetween(10,20);
        int j = faker.number().numberBetween(21,30);
        System.out.println("i = " + i);
        System.out.println("j = " + j);
        i += 2; // i = i + 2;
        System.out.println("i = " + i);
        i += 5;  // i = i + 5;
        System.out.println("i = " + i);
        j -= 4; // j = i - 4;
        System.out.println("j = " + j);
        String nombreDePila = persona.getNombrePila();
        nombreDePila += "-";
        nombreDePila += persona.getApellidoPaterno();
        nombreDePila += "-";
        nombreDePila += persona.getApellidoMaterno();
        System.out.println("nombreDePila = " + nombreDePila);
    }
}
