package org.zutjmx;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import kong.unirest.HttpResponse;
import org.zutjmx.models.*;
import org.zutjmx.util.*;

import java.io.IOException;


public class Main {
    public static void main(String[] args) {
        //getPistas(); //getDominios(); //getPersonas(); //getEmpleadosGerentes(); //getNbaPlayers();
        System.out.println(":::: Operadores Lógicos ::::");
        OperadoresLogicos operadoresLogicos = new OperadoresLogicos();
        operadoresLogicos.manejoOperadoresLogicos();
        //Fibonacci fibonacci = new Fibonacci();
        //fibonacci.ejecutaSerie();
        //int resultadoFib = fibonacci.getFib(5);
        //System.out.println("resultadoFib = " + resultadoFib);
    }

    private static void getPistas() {
        System.out.println(":::: Prueba para llamar el endpoint  http://jservice.io/api/clues :::::");

        String respuesta = "";
        ClienteHttp clienteHttp = new ClienteHttp();
        try {
            respuesta = clienteHttp.recuperaPistas();
        } catch (IOException e) {
            throw new RuntimeException(e);
        }

        deserializaPistas(respuesta);
    }

    private static void deserializaPistas(String respuesta) {
        if (respuesta.length() > 0) {
            System.out.println("respuesta = " + respuesta);
            ObjectMapper om = new ObjectMapper();
            try {
                Pista[] pistas = om.readValue(respuesta, Pista[].class);
                System.out.println("pistas.length = " + pistas.length);
                for (int i = 0; i < pistas.length; i++) {
                    System.out.println("pistas[" + i + "] = " + pistas[i].toString());
                }
            } catch (JsonProcessingException e) {
                throw new RuntimeException(e);
            }
        }
    }

    private static void getDominios() {
        System.out.println(":::: Prueba para llamar el endpoint  https://api.domainsdb.info/v1/domains/search :::::");
        ClienteHttp clienteHttp = new ClienteHttp();
        HttpResponse<String> response = clienteHttp.busquedaDominios();
        deserializaDominios(response.getBody().toString());
    }

    private static void deserializaDominios(String respuesta) {
        if (respuesta.length() > 0) {
            System.out.println("respuesta = " + respuesta);
            ObjectMapper om = new ObjectMapper();
            try {
                RootDomain rootDomain = om.readValue(respuesta, RootDomain.class);
                Domain[] domains = rootDomain.domains.toArray(new Domain[0]);
                System.out.println("domains.length = " + domains.length);
                for (int i = 0; i < domains.length; i++) {
                    System.out.println("domains[" + i + "] = " + domains[i].toString());
                }
            } catch (JsonProcessingException e) {
                throw new RuntimeException(e);
            }
        }
    }

    private static void getNbaPlayers() {
        System.out.println(":::: Prueba para llamar el endpoint  https://api.domainsdb.info/v1/domains/search :::::");
        ClienteHttp clienteHttp = new ClienteHttp();
        String jugadores = clienteHttp.recuperaNbaPlayers();
        deserializaJugadores(jugadores);
    }

    private static void deserializaJugadores(String respuesta) {
        if(respuesta.length() > 0) {
            System.out.println("respuesta = " + respuesta);
            ObjectMapper om = new ObjectMapper();
            try {
                RespuestaJugadoresNba respuestaJugadoresNba = om.readValue(respuesta, RespuestaJugadoresNba.class);
                Jugador[] jugadores = respuestaJugadoresNba.data.toArray(new Jugador[0]);
                System.out.println("jugadores.length = " + jugadores.length);
                for (int i = 0; i < jugadores.length; i++) {
                    System.out.println("jugadores[" + i + "] = " + jugadores[i].toString());
                }
            } catch (JsonProcessingException e) {
                throw new RuntimeException(e);
            }

        }
    }

    private static void getPersonas() {
        System.out.println(":::: Recuperando datos de la tabla Personas Sql Server ::::");
        SqlServerData sqlServerData = new SqlServerData();
        sqlServerData.getDatosPersona();
    }

    private static void getEmpleadosGerentes() {
        System.out.println(":::: Recuperando datos del SP uspGetEmployeeManagers Sql Server ::::");
        SqlServerData sqlServerData = new SqlServerData();
        sqlServerData.getEmployeeManagers();
    }
}