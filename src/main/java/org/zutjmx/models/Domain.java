package org.zutjmx.models;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.ArrayList;
import java.util.Date;

public class Domain {
    public String domain;
    public Date create_date;
    public Date update_date;
    public String country;
    public String isDead;
    @JsonProperty("A")
    public ArrayList<String> a;
    @JsonProperty("NS")
    public ArrayList<String> nS;
    @JsonProperty("CNAME")
    public ArrayList<String> cNAME;
    @JsonProperty("MX")
    public ArrayList<MX> mX;
    @JsonProperty("TXT")
    public ArrayList<String> tXT;

    public Domain() {
    }

    public Domain(String domain,
                  Date create_date,
                  Date update_date,
                  String country,
                  String isDead,
                  ArrayList<String> a,
                  ArrayList<String> nS,
                  ArrayList<String> cNAME,
                  ArrayList<MX> mX,
                  ArrayList<String> tXT) {
        this.domain = domain;
        this.create_date = create_date;
        this.update_date = update_date;
        this.country = country;
        this.isDead = isDead;
        this.a = a;
        this.nS = nS;
        this.cNAME = cNAME;
        this.mX = mX;
        this.tXT = tXT;
    }

    public String getDomain() {
        return domain;
    }

    public void setDomain(String domain) {
        this.domain = domain;
    }

    public Date getCreate_date() {
        return create_date;
    }

    public void setCreate_date(Date create_date) {
        this.create_date = create_date;
    }

    public Date getUpdate_date() {
        return update_date;
    }

    public void setUpdate_date(Date update_date) {
        this.update_date = update_date;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getIsDead() {
        return isDead;
    }

    public void setIsDead(String isDead) {
        this.isDead = isDead;
    }

    public ArrayList<String> getA() {
        return a;
    }

    public void setA(ArrayList<String> a) {
        this.a = a;
    }

    public ArrayList<String> getnS() {
        return nS;
    }

    public void setnS(ArrayList<String> nS) {
        this.nS = nS;
    }

    public ArrayList<String> getcNAME() {
        return cNAME;
    }

    public void setcNAME(ArrayList<String> cNAME) {
        this.cNAME = cNAME;
    }

    public ArrayList<MX> getmX() {
        return mX;
    }

    public void setmX(ArrayList<MX> mX) {
        this.mX = mX;
    }

    public ArrayList<String> gettXT() {
        return tXT;
    }

    public void settXT(ArrayList<String> tXT) {
        this.tXT = tXT;
    }

    @Override
    public String toString() {
        return "Domain{" +
                "domain='" + domain + '\'' +
                ", create_date=" + create_date +
                ", update_date=" + update_date +
                ", country='" + country + '\'' +
                ", isDead='" + isDead + '\'' +
                ", a=" + a +
                ", nS=" + nS +
                ", cNAME=" + cNAME +
                ", mX=" + mX +
                ", tXT=" + tXT +
                '}';
    }
}
