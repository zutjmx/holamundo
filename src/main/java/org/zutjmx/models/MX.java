package org.zutjmx.models;

public class MX {
    public String exchange;
    public int priority;

    public MX() {
    }

    public MX(String exchange, int priority) {
        this.exchange = exchange;
        this.priority = priority;
    }

    public String getExchange() {
        return exchange;
    }

    public void setExchange(String exchange) {
        this.exchange = exchange;
    }

    public int getPriority() {
        return priority;
    }

    public void setPriority(int priority) {
        this.priority = priority;
    }
}
