package org.zutjmx.models;

import java.util.Date;

public class Persona {
    private String nombrePila;
    private String apellidoPaterno;
    private String ApellidoMaterno;
    private Date fechaNacimiento;

    public Persona() {
    }

    public Persona(String nombrePila, String apellidoPaterno, String apellidoMaterno, Date fechaNacimiento) {
        this.nombrePila = nombrePila;
        this.apellidoPaterno = apellidoPaterno;
        ApellidoMaterno = apellidoMaterno;
        this.fechaNacimiento = fechaNacimiento;
    }

    public String getNombrePila() {
        return nombrePila;
    }

    public void setNombrePila(String nombrePila) {
        this.nombrePila = nombrePila;
    }

    public String getApellidoPaterno() {
        return apellidoPaterno;
    }

    public void setApellidoPaterno(String apellidoPaterno) {
        this.apellidoPaterno = apellidoPaterno;
    }

    public String getApellidoMaterno() {
        return ApellidoMaterno;
    }

    public void setApellidoMaterno(String apellidoMaterno) {
        ApellidoMaterno = apellidoMaterno;
    }

    public Date getFechaNacimiento() {
        return fechaNacimiento;
    }

    public void setFechaNacimiento(Date fechaNacimiento) {
        this.fechaNacimiento = fechaNacimiento;
    }

    @Override
    public String toString() {
        return "Persona { " +
                "nombrePila='" + nombrePila + '\'' +
                ", apellidoPaterno = '" + apellidoPaterno + '\'' +
                ", ApellidoMaterno = '" + ApellidoMaterno + '\'' +
                ", fechaNacimiento = " + fechaNacimiento +
                " }";
    }
}
