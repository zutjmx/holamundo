package org.zutjmx.models;

import java.util.ArrayList;

public class RootDomain {
    public ArrayList<Domain> domains;
    public int total;
    public String time;
    public Object next_page;

    public RootDomain() {
    }

    public RootDomain(ArrayList<Domain> domains, int total, String time, Object next_page) {
        this.domains = domains;
        this.total = total;
        this.time = time;
        this.next_page = next_page;
    }

    public ArrayList<Domain> getDomains() {
        return domains;
    }

    public void setDomains(ArrayList<Domain> domains) {
        this.domains = domains;
    }

    public int getTotal() {
        return total;
    }

    public void setTotal(int total) {
        this.total = total;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public Object getNext_page() {
        return next_page;
    }

    public void setNext_page(Object next_page) {
        this.next_page = next_page;
    }

    @Override
    public String toString() {
        return "RootDomain{" +
                "domains=" + domains +
                ", total=" + total +
                ", time='" + time + '\'' +
                ", next_page=" + next_page +
                '}';
    }
}
