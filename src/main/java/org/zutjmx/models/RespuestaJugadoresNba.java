package org.zutjmx.models;

import java.util.ArrayList;

public class RespuestaJugadoresNba {
    public ArrayList<Jugador> data;
    public Meta meta;

    public RespuestaJugadoresNba() {
    }

    public RespuestaJugadoresNba(ArrayList<Jugador> jugadores, Meta meta) {
        this.data = jugadores;
        this.meta = meta;
    }

    public ArrayList<Jugador> getJugadores() {
        return data;
    }

    public void setJugadores(ArrayList<Jugador> jugadores) {
        this.data = jugadores;
    }

    public Meta getMeta() {
        return meta;
    }

    public void setMeta(Meta meta) {
        this.meta = meta;
    }
}
