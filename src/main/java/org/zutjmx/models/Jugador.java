package org.zutjmx.models;

public class Jugador {
    public int id;
    public String first_name;
    public int height_feet;
    public int height_inches;
    public String last_name;
    public String position;
    public Team team;
    public int weight_pounds;

    public Jugador() {
    }

    public Jugador(int id, String first_name, int height_feet, int height_inches, String last_name, String position, Team team, int weight_pounds) {
        this.id = id;
        this.first_name = first_name;
        this.height_feet = height_feet;
        this.height_inches = height_inches;
        this.last_name = last_name;
        this.position = position;
        this.team = team;
        this.weight_pounds = weight_pounds;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getFirst_name() {
        return first_name;
    }

    public void setFirst_name(String first_name) {
        this.first_name = first_name;
    }

    public int getHeight_feet() {
        return height_feet;
    }

    public void setHeight_feet(int height_feet) {
        this.height_feet = height_feet;
    }

    public int getHeight_inches() {
        return height_inches;
    }

    public void setHeight_inches(int height_inches) {
        this.height_inches = height_inches;
    }

    public String getLast_name() {
        return last_name;
    }

    public void setLast_name(String last_name) {
        this.last_name = last_name;
    }

    public String getPosition() {
        return position;
    }

    public void setPosition(String position) {
        this.position = position;
    }

    public Team getTeam() {
        return team;
    }

    public void setTeam(Team team) {
        this.team = team;
    }

    public int getWeight_pounds() {
        return weight_pounds;
    }

    public void setWeight_pounds(int weight_pounds) {
        this.weight_pounds = weight_pounds;
    }

    @Override
    public String toString() {
        return "Jugador{" +
                "id=" + id +
                ", first_name='" + first_name + '\'' +
                ", height_feet=" + height_feet +
                ", height_inches=" + height_inches +
                ", last_name='" + last_name + '\'' +
                ", position='" + position + '\'' +
                ", team=" + team.toString() +
                ", weight_pounds=" + weight_pounds +
                '}';
    }
}
